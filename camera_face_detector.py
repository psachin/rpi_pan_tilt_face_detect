from imutils.video import VideoStream
import imutils
import time
import cv2

detector = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

print(f"[INFO] Starting video stream...")
vs = VideoStream(src=0).start()
time.sleep(2.0)                 # camera warmup

from pid.pid import PID
pid = PID(kP=0.5, kI=0, kD=0)
pid.initialize()

from driver.servo_pigpio import Servo
pan = Servo(2)
tilt = Servo(3)
pan.initialize()
tilt.initialize()
time.sleep(1)

def servo_reset():
    pan.set_angle(90)
    tilt.set_angle(90)

servo_reset()

def servo_position(x=0, y=0):
    df = 40
    if pan.get_position() < x:
        x = pan.get_position() + df
        if x >= 2500:
            print('x: limit')
            x = 2500
        pan.sweep(df)

    if pan.get_position() > x:
        x = pan.get_position() - df
        if x <= 500:
            print('x: limit')
            x = 500
        pan.sweep(-df)

    if tilt.get_position() < y:
        y = tilt.get_position() + df
        if y >= 2500:
            y = 2500
        tilt.sweep(df)

    if tilt.get_position() > y:
        y = tilt.get_position() - df
        if y <= 500:
            y = 500
        tilt.sweep(-df)

    # pan_servo_position = pan.get_position()
    # pan.sweep(x - pan_servo_position)

    # tilt_servo_position = tilt.get_position()
    # tilt.sweep(y - tilt_servo_position)


def __servo_position(x=0, y=0):
    df = 40
    if pan.get_position() < x:
        x = pan.get_position() + df
        if x >= 2500:
            print('x: limit')
            x = 2500
        pan.set_position(x)

    if pan.get_position() > x:
        x = pan.get_position() - df
        if x <= 500:
            print('x: limit')
            x = 500
        pan.set_position(x)

    if tilt.get_position() < y:
        y = tilt.get_position() + df
        if y >= 2500:
            y = 2500
        tilt.set_position(y)

    if tilt.get_position() > y:
        y = tilt.get_position() - df
        if y <= 500:
            y = 500
        tilt.set_position(y)


while True:
    frame = vs.read()
    frame = imutils.resize(frame, width=400)
    frame = cv2.flip(frame, 1)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    rects = detector.detectMultiScale(gray, scaleFactor=1.05, minNeighbors=5, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
    # if rects:
    #     print(rects)
    for (x, y, w, h) in rects:
        #cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
        move_x = (5 * x) + 500
        move_y = (6.66 * y) + 500
        # print(move_x, move_y)
        # move_x = pid.update(move_x)
        # move_y = pid.update(move_y)
        # print(move_x, move_y)
        servo_position(move_x, move_y)

    # cv2.imshow("frame", frame)
    key = cv2.waitKey(1) & 0xFF

    if key == ord("q"):
        break

cv2.destroyAllWindows()
vs.stop()
