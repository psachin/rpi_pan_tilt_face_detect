import time

class PID:
    def __init__(self, kP=1, kI=0, kD=0):
        # constants
        self.kP = kP
        self.kI = kI
        self.kD = kD


    def initialize(self):
        self.curr_time = time.time()
        self.prev_time = self.curr_time

        # previous error
        self.prev_error = 0

        # initialize the result variables
        self.cP = 0
        self.cI = 0
        self.cD = 0


    def update(self, error, sleep=0.2):
        # puse for a bit
        time.sleep(sleep)

        # calculate delta time & delta error
        self.curr_time = time.time()
        delta_time = self.curr_time = self.prev_time
        delta_error = error - self.prev_error

        # P: If the error is large, the output will be proportionally large
        self.cP = error

        # I: If the error is reduced over time this term won't grow
        self.cI += error * delta_time

        # D: If P or I values overshoot, this will reduce it as this
        # does not consider the magnitude of the error but he rate of change of error.
        self.cD = (delta_error / delta_time) if delta_time > 0 else 0

        # save previous time & error
        self.prev_time = self.curr_time
        self.prev_erro = error

        # return sum of the terms
        return sum([
            self.kP * self.cP,
            self.kI * self.cI,
            self.kD * self.cD
        ])
