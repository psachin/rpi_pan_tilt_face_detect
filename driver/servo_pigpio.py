'''Using pigpio to avoid servo jitter.'''
import RPi.GPIO as GPIO
import pigpio
from time import sleep


class Servo:
    def __init__(self, pin, freq=50):
        self.pin = pin
        self.freq = freq
        self.min_range = 500
        self.max_range = 2500

    def calculate_position(self, angle):
        """
        Positions for freq = 50 Hz.
        ---------------------------
        0 deg: 500
        90 deg: 1500
        180 deg: 2500
        ---------------------------

        1000/90=11.11 i.e 11.11 is 1 degree
        """
        if angle > 0 and angle < 181:
            pos = (angle * (1000/90)) + 500
            # if pos > 2500:
            #      return 2500
            # else:
            return pos
        elif angle == 0:
            return 500

    def initialize(self):
        self.pwm = pigpio.pi()
        self.pwm.set_mode(self.pin, pigpio.OUTPUT)
        self.pwm.set_PWM_frequency(self.pin, self.freq)

    def set_angle(self, angle):
        """Position at 90 degree"""
        self.pwm.set_servo_pulsewidth(self.pin, self.calculate_position(angle))

    def center_position(self):
        self.pwm.set_servo_pulsewidth(self.pin, 1500)
        sleep(1)

    def get_position(self):
        return self.pwm.get_servo_pulsewidth(self.pin)

    def set_position(self, pos):
        '''Set absolute position
        '''
        if pos >= self.min_range and pos <= self.max_range:
            self.pwm.set_servo_pulsewidth(self.pin, pos)
        else:
            print(f'OUT OF SCOPE: {pos}, centering...')
            # self.center_position()

        return self.get_position()

    def move(self, pos):
        '''Move forward (+), backward (-) from current position.
        '''
        self.pos = self.get_position()
        self.pos += pos

        if self.pos >= self.min_range and self.pos <= self.max_range:
            self.pwm.set_servo_pulsewidth(self.pin, self.pos)
        else:
            print(f"Out of scope")
            # self.center_position()

        return self.get_position()

    def sweep(self, steps:int):
        if steps < 0:
            for i in range(0, int(abs(steps))):
                self.move(-1)
        else:
            for i in range(0, int(steps)):
                self.move(1)


if __name__ == '__main__':
    # servo check
    pan = Servo(2)
    tilt = Servo(3)
    pan.initialize()
    tilt.initialize()
    sleep(1)

    while True:
        pan.set_position(500)
        tilt.set_position(500)
        for i in range(2000):
            pan.move(1)
            tilt.move(1)

        pan.center_position()
        tilt.center_position()
        sleep(1)

        pan.set_position(2500)
        tilt.set_position(2500)
        for i in range(2000):
            pan.move(-1)
            tilt.move(-1)
        pan.center_position()
        tilt.center_position()
        sleep(1)
