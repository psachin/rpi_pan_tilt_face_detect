import RPi.GPIO as GPIO
import pigpio
from time import sleep


class ServoPosition:
    def __init__(self, pan_servo=2, tilt_servo=3):
        self.pan_servo = pan_servo
        self.tilt_servo = tilt_servo
        
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(self.pan_servo, GPIO.OUT)
        GPIO.setup(self.tilt_servo, GPIO.OUT)

        self.x = GPIO.PWM(self.pan_servo, 50)
        self.y = GPIO.PWM(self.tilt_servo, 50)

    def initialize(self):
        self.currentx = 0
        self.currenty = 0
        self.x.start(self.currentx)
        self.y.start(self.currenty)
        sleep(1)

    def reset_position(self, x=7, y=7):
        self.set_dc_x(x)
        self.set_dc_y(y)

    def set_position_x(self, diffx):
        self.currentx += diffx
        self.currentx = round(self.currentx, 2)

        if self.currentx < 13 and self.currentx > 1:
            self.x.ChangeDutyCycle(self.currentx)

    def set_position_y(self, diffy):
        self.currenty += diffy
        self.currenty = round(self.currenty, 2)

        if self.currenty < 13 and self.currenty > 1:
            self.y.ChangeDutyCycle(self.currenty)

    def set_dc_x(self, dcx):
        self.x.ChangeDutyCycle(dcx)

    def set_dc_y(self, dcy):
        self.y.ChangeDutyCycle(dcy)

    def stop(self):
        self.x.stop()
        self.y.stop
        GPIO.cleanup()


if __name__ == '__main__':
    ser = ServoPosition()
    ser.initialize()
    ser.reset_position()
    ser.set_dc_x(12)
    ser.set_dc_y(12)
    ser.stop()
    
    # ser.set_position_x(4)
    # ser.set_position_y(4)
